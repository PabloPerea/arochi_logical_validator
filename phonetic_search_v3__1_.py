from abydos.phonetic import Phonem, Metaphone, MRA, Phonix, FONEM, SoundD, MetaSoundex, NRL, PhoneticSpanish,SpanishMetaphone
from nltk.corpus import stopwords
import nltk
nltk.download('stopwords')
import re
import jaro
import csv
import pandas as pd
import json


class phoneticSearch:
    
    def __init__(self):
        self.phm = Phonem()
        self.mt = Metaphone()
        self.mra = MRA()
        self.phx = Phonix()
        self.fon = FONEM()
        self.sd = SoundD()
        self.mets = MetaSoundex()
        self.nrl = NRL()
        self.spanish=PhoneticSpanish()
        self.spmt=SpanishMetaphone()
        self.algorithms=[self.phm,self.mt,self.mra,self.phx,self.fon,self.sd,self.mets,self.nrl,self.spanish,self.spmt]
        self.algorithms_2 = ["phm", "mt","mra", "phx", "fon", "sd", "mets","nrl","spanish","spmt"]
        
    
    def busqueda_por_eliminacion(self,code1,word1,code2,word2):
        palabra1=word1
        palabra2=word2
        lista_vocales="aeiou"
        for i in range(len(code1)):
            letra_to_remove=code1[i].lower()
            if letra_to_remove not in lista_vocales: 
                palabra1=re.sub(letra_to_remove,'',palabra1)

        for j in range(len(code2)):
            letra_to_remove2=code2[j].lower()
            if letra_to_remove2 not in lista_vocales:
                palabra2=re.sub(letra_to_remove2,'',palabra2)

        if code1 in code2 or code2 in code1:
            if palabra1.lower() in palabra2.lower() or palabra2.lower() in palabra1.lower(): 
                #print("son palabras parecidas")
                return True
            else:
                return False
        else:
            return False
    
    def obtencion_codigos(self,word1,word2,algoritmo):
        if "z" in word1:
            word1=re.sub('z','s',word1)
        if "v" in word1:
            word1=re.sub('v','b',word1)
        if "c" in word1:
            indice=word1.find("c")
            if indice+1 < len(word1):
                if word1[indice+1]== "e" or word1[indice+1]=="i":
                    word1=re.sub('c','s',word1)
                else:
                    word1=re.sub('c','k',word1)
            elif indice == (len(word1)-1) :
                word1=word1[:-1]
                word1=word1+'k'
      
        
        if "z" in word2:
            word2=re.sub('Z','S',word2)
        if "v" in word2:
            word2=re.sub('V','B',word2)
        if "c" in word2:
            indice2=word2.find("c")
            if indice2+1 < len(word2):
                if word2[indice2+1]== "e" or word2[indice2+1]=="i":
                    word2=re.sub('C','S',word2)
                else:
                    word2=re.sub("c","k",word2)
            elif indice == (len(word2)-1):
                word2=word2[:-1]
                word1=word1+'k'
                    
        code1=algoritmo.encode(word1)
        code2=algoritmo.encode(word2)
        
        return code1,code2

    def validacion_booleana(self,word1,word2):
        lista_verdaderos=[]
        for w in word1.split(" "):
            for w2 in word2.split(" "):
                code1,code2=self.obtencion_codigos(w,w2,self.mra)
                print(w,"::",code1,word1)
                print(w2,"::",code2,word2)
                respuesta=self.busqueda_por_eliminacion(code1,w,code2,w2)
                lista_verdaderos.append(respuesta)
        
        if True in lista_verdaderos:
            return True
        else:
            return False
    

    def phoneticTest(self,word1,word2):
        ratios_wd = {}
        for i,phonetic in enumerate(self.algorithms):
            ratios_w=[]
            for w in word1.split(" "):
                for w2 in word2.split(" "):       
                    code1,code2=self.obtencion_codigos(w,w2,phonetic)
                    jaroW=jaro.jaro_winkler_metric(code1,code2)
                    ratios_w.append(jaroW)
                    
                    
            ratios_wd[self.algorithms_2[i]]=ratios_w
        
            
            lista_promedios=[]
        for i,phonetic in enumerate(self.algorithms_2):
            ratios_wd[phonetic]=[x for x in ratios_wd[phonetic] if x!=0.0]
            if len(ratios_wd[phonetic]):
                promedio=sum(ratios_wd[phonetic])/len(ratios_wd[phonetic])
                lista_promedios.append(promedio)
        
                    
        if len(lista_promedios)<1:
            return 0.0
        else:              
            return max(lista_promedios)
        
    def carga_json(self):
        with open('stopwords_arochi.json') as file:
            data = json.load(file)

        return data['palabras']
    
    def limpiar_string(self,string):
        string=string.lower()
        caracteres_validos = "[A-Za-z0-9 ]"
        string_limpio = re.sub(r'[^' + caracteres_validos + ']', '', string)
        
        string_final=string_limpio
        # stop_words_spa = list(set(stopwords.words('spanish')))
        # stop_words_en = list(set(stopwords.words('english')))

        # for w in string_limpio.split(" "):
        #     if (w in stop_words_spa or w in stop_words_en) and len(string_limpio.split())>1:
        #         #print(len(string_limpio),string_limpio,len('ON'.split()))
        #         string_final=re.sub(w,"",string_limpio)
        palabras=self.carga_json()
        for w in string_limpio.split(" "):
            if w in palabras and len(string_limpio.split()>1):
                string_final=re.sub(w," ",string_limpio)
        
        

        return " ".join(string_final.split())
    
    
    def busqueda(self,palabra1,palabra2):
        #palabra1=palabra1.lower()
        #palabra2=palabra2.lower()
        palabra1=self.limpiar_string(palabra1)
        palabra2=self.limpiar_string(palabra2)
        if len(palabra1) > 1 and len(palabra2) > 1:
            respuesta_booleana=self.validacion_booleana(palabra1,palabra2)
            if not respuesta_booleana:
                score=self.phoneticTest(palabra1,palabra2)
                return score
            else:
                return respuesta_booleana
        else:
            return "la similitud no puede ser obtenida"
        
    
        
def write_csv(registros):
    with open('./algoritmo_pablo_coincidencias_v_final.csv', 'w', newline='') as csvfile:
        fieldnames = ['MarkNameIMPI', 'MarkNameArochi', 'target','Result Similarity']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for fila in registros:
            writer.writerow(fila) 
            
def get_data():
    datos = []
    df = pd.read_csv('coincidencias.csv')
    df = df[['MarkNameIMPI','MarkNameArochi','target']]
    for idx in df.index:
        if str(df.MarkNameIMPI[idx])=='nan' or str(df.MarkNameArochi[idx])=='nan':
            continue
        
        score = buscador.busqueda(df.MarkNameIMPI[idx],df.MarkNameArochi[idx])
        #break
        datos.append({'MarkNameIMPI':df.MarkNameIMPI[idx],'MarkNameArochi':df.MarkNameArochi[idx],'target':df.target[idx],'Result Similarity':score})
        
    df_2=pd.DataFrame(datos)
    df_2.to_csv("metricas_levi.csv",header=['MarkNameIMPI', 'MarkNameArochi', 'target','Result Similarity'])
    #write_csv(datos)
    print("Terminado") 
      

###########################################################################
#################################### main #################################
###########################################################################
buscador=phoneticSearch()
palabra1="boss"
palabra2="reckit & col the bossman"

respuesta=buscador.busqueda(palabra1,palabra2)
print(respuesta)

#get_data()

    
